#8. Reference Material

You can find some additional information here:

* [ASTRA Source Code](https://gitlab.com/astra-language/astra-core)
* [Maven Central](https://mvnrepository.com/artifact/com.astralanguage)
