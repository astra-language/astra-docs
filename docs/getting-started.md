#3. Getting Started with ASTRA

##3.1 Compiling ASTRA Code

ASTRA code is compiled using [Apache Maven](http://maven.apache.org) and the entire ASTRA codebase is available through [Maven Central](https://mvnrepository.com/artifact/com.astralanguage). New versions of ASTRA are released to this repository on an ad-hoc basis. The source code for ASTRA is available through [gitlab.com](https://gitlab.com/astra-language/).

An introductory guide to [Building & Deploying ASTRA Programs](deploying.md) is available. This guide will explain how to create a simple ASTRA project using maven.

##3.2 ASTRA and IDE Integration

While ASTRA can be used with any IDE that supports Maven, we have chosen to provide support for syntax highlighting for the [Visual Studio Code](https://code.visualstudio.com/) platform.  This support is delivered through a custom extension. To install the extension, simple click on the extensions tab and type `astra`.

If you wish to use another IDE, then you may be able to take advance of the following extensions:

* [Intellij Syntax Highlighting](https://github.com/mxschll/textmate-grammar-astra): contributed by Maximilian Scholl
* [Neovim Syntax Highlighting](https://github.com/denartha10/tree-sitter-astra): contributed by Niall Mahon

##3.3 ASTRA Examples

A number of example ASTRA programs can be found on [Gitlab.com](https://gitlab.com/astra-language/examples).
