#7. Video Tutorial

The Youtube videos on this page provide a basic introduction to programming with ASTRA.

##7.1. Introducing ASTRA

We start off with a basic introduction to ASTRA.

[![](https://markdown-videos-api.jorgenkh.no/youtube/g8dboEIO5Vk)](https://youtu.be/g8dboEIO5Vk)
