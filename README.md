ASTRA Agent Programming Language
================================

This project contains the source code for the ASTRA User Guides. Updates are automatically published to [http://guide.astralanguage.com](http://guide.astralanguage.com).
